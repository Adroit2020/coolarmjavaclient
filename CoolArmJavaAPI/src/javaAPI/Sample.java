/**
 * 
 */
package javaAPI;


import coppelia.FloatWA;
import coppelia.IntW;
import coppelia.FloatW;
/**
 *
 *
 */
public class Sample {
	
	public Sample() {
		
	}
	
	public static void main(String[] args) {
		int connectionStatus;
		CoolArmControl controllerObject = new CoolArmControl();
		System.out.println("Cool Arm Control Object created");
		
		connectionStatus  = controllerObject.connect();
		if(connectionStatus != 0)
		{
			System.out.println("Alert! Connection to dynamotion server not established.");
		}
		else
		{
			System.out.println("Connected Successfully");
		}
		
		controllerObject.enableRemoteJavaApiMode();
		
		//Set the COM port here if hardware is used. Not required just with simulation. 
			controllerObject.setCOMPort("7");
		
			
		//Enable Hardware
			controllerObject.enableHardwareMode();
		
		// Use the following strings to control different joints. 
		// for joint0, pass "joint_0" as argument
		// for joint1, pass "joint_1" as argument
		// for joint2, pass "joint_2" as argument
		// for joint3, pass "joint_3" as argument
		// for joint4, pass "joint_4" as argument;
		// for joint5, pass "joint_5" as argument
			float pos[] = {1.01f,-1.57f,1.01f,-1.101f,1.01f,1.01f};
			float vel[] = {1.01f,0.1f,1.01f,1.101f,1.01f,1.01f};
			
			controllerObject.setJointPositionAndVelocity(pos,vel);
		// Input arguments are the target joint angle of a specific joint in radians. Forward Kinematics
			//IntW objectHandler = controllerObject.getObjectHandle("joint_1");
			//controllerObject.setJointPosition(objectHandler, 1.0f);
			
			
			//currentJointPosition = controllerObject.getJointPosition(objectHandler);
			//System.out.format("Current Joint Position %f\n",currentJointPosition.getValue());
		
		//Controlling End Effector position and orientation through IK
		// Enable Inverse Kinematics in the Scene. Screenshots and directions for this are already provided. 
			//IntW objectHandlerEndEffector = controllerObject.getObjectHandle("manipulationSphere");
			//controllerObject.setEndEffectorPosition(objectHandlerEndEffector.getValue(), 0.0f, 0.0f, -0.0f);
			//controllerObject.setEndEffectorOrientation(objectHandlerEndEffector.getValue(), 1.57f, 1.57f, 1.57f);
		
		// Controlling the gripper
			//IntW objectHandle1 = controllerObject.getObjectHandle("joint_6");
			//IntW objectHandle2 = controllerObject.getObjectHandle("joint_7");
			//controllerObject.moveGripper(objectHandle1,objectHandle2,0.15f);
		
		/* Below are the different modes: enabling record, playback, hardwareEnable, assistive and disable functions for each.
		 Prior to running these modes the remote Java API button should be enabled through the below buttons.
		 These functions have been tested individually for checking their working with the hardware but the 
		 code has to be developed in order to make the record and playback work altogether through these API's.*/
		
			//controllerObject.disableHardwareMode();
			
		/* Logic to be developed: Check whether the button is in pressed state or not(Please refer Vrep functionality simxGetButtonProperty
		 * More info about the API in http://www.coppeliarobotics.com/helpFiles/en/remoteApiFunctionsJava.htm*/	
			//controllerObject.enableRecordMode();
			//controllerObject.disableRecordMode();
			//controllerObject.disableAssistiveMode();
			//controllerObject.enablePlaybackMode();
			//controllerObject.disablePlaybackMode();			
			//controllerObject.enableAssistiveMode();
			// Enter the file path and the file name as given below to play the recorded motion.
			//controllerObject.enablePlaybackMode("C:/Users/Lenovo/Desktop/record.dat");
			
			//controllerObject.setJointVelocity(200);
			
			
	}
}
